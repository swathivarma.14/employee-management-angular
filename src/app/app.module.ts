import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { TabsModule } from "ngx-bootstrap/tabs";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TruncatePipe} from './transform.pipe';
import { NumberDirective } from '../../src/utils/NumberDirective';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    TruncatePipe,
    NumberDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    TabsModule,
    NgbModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
