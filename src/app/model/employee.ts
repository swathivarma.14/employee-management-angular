export class Employee {
    id: number=0;
    name: string='';
    designation: string='';
    salary: number=0;
    bio: string='';
    dob: string='';
}
