import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms'
import { Employee } from 'src/app/model/employee';
import { EmployeeService } from 'src/app/service/employee.service';
import { TruncatePipe  } from '../../transform.pipe';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  empDetails !: FormGroup;
  empObj : Employee = new Employee();
  employeeData !: any;
  showAdd !: boolean;
  showUpdate !: boolean;
  add !: any;

  constructor(private formBuilder: FormBuilder, private api: EmployeeService) { }

  ngOnInit(): void {
    this.empDetails = this.formBuilder.group({
      name: [''],
      salary: [''],
      designation: [''],
      bio: [''],
      dob: ['']
    });
    const data = sessionStorage.getItem('tab')
    this.storeData(data);
    this.getEmployeeDetails();
  }

  clickAddEmployee () {
    this.empDetails.reset();
    this.showAdd = true;
    this.showUpdate = false;
  }

  addEmployee () {
    this.empObj.name = this.empDetails.value.name;
    this.empObj.designation = this.empDetails.value.designation;
    this.empObj.salary = this.empDetails.value.salary;
    this.empObj.bio = this.empDetails.value.bio;
    this.empObj.dob = this.empDetails.value.dob;

    this.api.postEmployee(this.empObj)
      .subscribe((res: any) => {
        alert("Employee Added Successfully");
        let ref = document.getElementById("cancel");
        ref?.click();
        this.empDetails.reset();
        this.getEmployeeDetails();
    }, err=> {      
      alert("Something went wrong");
    })
  }

  getEmployeeDetails () {
    this.api.getEmployee()
    .subscribe(res => {
      this.employeeData = res;
    })
  }

  deleteEmployee (row: any) {
    this.api.deleteEmployee(row.id)
    .subscribe(res => {
      alert("Employee deleted");
      this.getEmployeeDetails();
    })
  }

  onEdit (row: any) {    
    this.showAdd = false;
    this.showUpdate = true;
    this.empObj.id = row.id;

    this.empDetails.controls['name'].setValue(row.name);
    this.empDetails.controls['designation'].setValue(row.designation);
    this.empDetails.controls['salary'].setValue(row.salary);
    this.empDetails.controls['bio'].setValue(row.bio);
    this.empDetails.controls['dob'].setValue(row.dob);
  }

  updateEmployee () {    
    this.empObj.name = this.empDetails.value.name;
    this.empObj.designation = this.empDetails.value.designation;
    this.empObj.salary = this.empDetails.value.salary;
    this.empObj.bio = this.empDetails.value.bio;
    this.empObj.dob = this.empDetails.value.dob;

    this.api.updateEmployee(this.empObj, this.empObj.id)
      .subscribe(res => {
        alert("Updated Sucessfully");
        let ref = document.getElementById("cancel");
        ref?.click();
        this.empDetails.reset();
        this.getEmployeeDetails();
      })
  }

  storeData(id: any) {
    sessionStorage.setItem("tab", JSON.stringify(id));
    this.add = id;
  }


}
