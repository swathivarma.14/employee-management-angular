# Employee-Management-Angular

Employee management project

## Getting started

Install node modules and run the project

```bash
npm i
# and
ng serve
```

Install json-server

```bash
npm install -g json-server
# and run
npx json-server --watch db.json
```
Json server will start in **http://localhost:3000/posts**

Redirect to **http://localhost:4200/**

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

